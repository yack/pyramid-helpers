# pyramid-helpers -- Helpers to develop Pyramid applications
# By: Cyril Lacoux <clacoux@easter-eggs.com>
#     Valéry Febvre <vfebvre@easter-eggs.com>
#
# Copyright (C) Cyril Lacoux, Easter-eggs
# https://gitlab.com/yack/pyramid-helpers
#
# This file is part of pyramid-helpers.
#
# pyramid-helpers is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# pyramid-helpers is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

[tool.poetry]
name = "pyramid-helpers"
version = "2.0.0"
description = "Helpers to develop Pyramid applications"
authors = [
    "Cyril Lacoux <clacoux@easter-eggs.com>",
    "Valéry Febvre <vfebvre@easter-eggs.com>",
]
license = "AGPL-3.0-or-later"
readme = "README.md"
repository = "https://gitlab.com/yack/pyramid-helpers"
keywords = ["web", "wsgi", "pylons", "pyramid", "helpers"]
homepage = "https://gitlab.com/yack/pyramid-helpers"
classifiers = [
    "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    "Development Status :: 5 - Production/Stable",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Programming Language :: Python :: 3.13",
    "Framework :: Pyramid",
    "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    "Topic :: Internet :: WWW/HTTP :: WSGI",
    "Topic :: Internet :: WWW/HTTP",
    "Intended Audience :: Developers",
]
include = [
    "pyramid_helpers/locale/*/LC_MESSAGES/*.mo",
    "pyramid_helpers/static/translations/*.js"
]
exclude = [
    "pyramid_helpers/locale/*.pot",
    "pyramid_helpers/locale/*/LC_MESSAGES/*.po",
    "pyramid_helpers/static/translations/.gitignore",
]

[tool.poetry.scripts]
phelpers-init-db = "pyramid_helpers.scripts.initializedb:main"

[tool.poetry.plugins."paste.app_factory"]
main = "pyramid_helpers:main"

[tool.poetry.dependencies]
babel = "^2.9"
colorlog = "^6.0"
decorator = "^5.0"
formencode = "^2.0"
mako = "^1.1"
markdown = { version = "^3.7", optional = true }
passlib = "^1.7"
pyrad = { version = "^2.3", optional = true }
pyramid = "^2.0"
pyramid-beaker = "^0.9"
pyramid-exclog = "^1.0"
pyramid-mako = "^1.1"
pyramid-tm = "^2.4"
python = "^3.9"
python-ldap = {version = "^3.4", optional = true }
transaction = "^5.0"
webob = "^1.8"

[tool.poetry.dev-dependencies]
flake8 = "*"
gunicorn = "^23.0"
invoke = "^2.0"
paste = "^3.5"
po2json = "^0.2.2"
pylint = "*"
pyramid-debugtoolbar = "^4.8"
pyramid-ipython = "^0.2"
pytest = "^8.0"
pytest-cov = "*"
pytest-steps = "^1.7"
robber = "^1.1"
sqlalchemy = "^2.0"
webtest = "^3.0"
"zope.sqlalchemy" = "^3.0"

[tool.poetry.extras]
api-doc = ["markdown"]
auth-ldap = ["python-ldap"]
auth-radius = ["pyrad"]

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
