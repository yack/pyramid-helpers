<%inherit file="../site.mako" />
<h2>${translate('Articles')}</h2>
<div class="partial-block position-relative" data-partial-key="articles.partial">
<%include file="list.mako" />
</div>
