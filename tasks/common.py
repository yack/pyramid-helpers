""" Common functions for Invoke tasks """

import os
import subprocess

from termcolor import cprint


def load_environ(filepath, reset=True):
    """
    Load environment variables from file

    @param string filepath: Path of the file to load environment variables from
    @param boolean reset: Whether to reset environment or not before loading the environment variables (Default is to reset the environment)
    """

    if not filepath:
        return

    cmd = f'set -a && . {filepath} && env -0'
    if reset:
        cmd = f'env -i sh -c "{cmd}"'

    output = subprocess.getoutput(cmd)

    os.environ.update(dict(
        line.split('=', 1)
        for line in output.split('\x00')
        if '=' in line
    ))


def print_title(title):
    """ Print a colored title """

    cprint(f'\n* {title.strip()}', 'blue')
