""" I18n Invoke tasks """

# pylint: disable=redefined-builtin,unused-argument

from invoke import task

from tasks.common import print_title

PREFIX = 'pyramid_helpers'
DOMAIN = 'pyramid-helpers'


@task
def compile(context):
    """Compile message catalogs to MO files."""

    print_title(compile.__doc__)

    context.run(f'pybabel compile --directory={PREFIX}/locale --domain={DOMAIN} --statistics', pty=True)


@task
def extract(context):
    """Extract messages from source files and generate a POT file."""

    print_title(extract.__doc__)

    context.run(f'pybabel extract -F babel.cfg --keyword=translate --keyword=pluralize:1,2 --output-file={PREFIX}/locale/{DOMAIN}.pot --width=83 {PREFIX}', pty=True)


@task(help={'locale': 'Short name of the locale to generate PO file for.'})
def init(context, locale):
    """Create new message catalogs from a POT file."""

    print_title(init.__doc__)

    context.run(f'pybabel init --domain={DOMAIN} --input-file={PREFIX}/locale/{DOMAIN}.pot --output-dir={PREFIX}/locale --locale={locale}', pty=True)


@task
def to_json(context):
    """Compile JSON from PO file"""

    print_title(to_json.__doc__)

    context.run(f'po2json {PREFIX}/locale/ {PREFIX}/static/translations/ {DOMAIN}', pty=True)


@task
def update(context):
    """Update existing message catalogs from a POT file."""

    print_title(update.__doc__)

    context.run(f'pybabel update --domain={DOMAIN} --input-file={PREFIX}/locale/{DOMAIN}.pot --output-dir={PREFIX}/locale --previous --width=83', pty=True)


@task(compile, to_json)
def generate(context):
    """Run the i18n generation pipeline."""


@task(update, generate, default=True)
def all(context):
    """Run the i18n pipeline."""
