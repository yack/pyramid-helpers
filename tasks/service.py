""" Service Invoke tasks """

from invoke import task

from tasks.common import load_environ
from tasks.common import print_title


@task
def httpd(context, env=None, reset_env=True):
    """Run the development HTTP server."""

    print_title(httpd.__doc__)

    load_environ(env, reset=reset_env)

    context.run('gunicorn $GUNICORN_OPTS', pty=True)
