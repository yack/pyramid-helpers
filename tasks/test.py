""" Test Invoke tasks """

# pylint: disable=redefined-builtin,unused-argument

import os

from invoke import task

from tasks.common import print_title


@task()
def eslint(context):
    """ Running eslint tests """

    print_title(eslint.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('eslint pyramid_helpers/static/js > build/eslint.txt', pty=True)


@task()
def flake8(context):
    """ Running flake8 tests """

    print_title(flake8.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('flake8 pyramid_helpers tasks tests > build/flake8.txt', pty=True)


@task()
def functional(context, test=None):
    """ Running functional tests """

    print_title(functional.__doc__)

    os.makedirs('build', exist_ok=True)

    cmd = 'pytest --disable-warnings --verbose --cov-report=html:build/coverage --cov-report=term --cov-report=xml:build/coverage.xml --cov=pyramid_helpers --junit-xml=build/pytest.xml tests/'
    if test:
        cmd = f'{cmd}{test}'

    context.run(cmd, pty=True)


@task()
def pylint(context):
    """ Running pylint tests """

    print_title(pylint.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('pylint pyramid_helpers tasks tests > build/pylint.txt', pty=True)


@task(eslint, flake8, pylint)
def static(context):
    """ Run the static tests pipeline """


@task(static, functional, default=True)
def all(context):
    """ Run the test pipeline """
