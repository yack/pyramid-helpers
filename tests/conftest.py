""" Tests functions for Pyramid-Helpers """

from copy import deepcopy
from io import StringIO
import select
from threading import Thread

from ldap.dn import explode_dn
import pytest

from pyrad.server import RemoteHost
from pyrad.server import Server
from pyrad.server import ServerPacketError
from pyrad.dictionary import Dictionary
from pyrad.packet import AccessAccept
from pyrad.packet import AccessReject
from pyrad.packet import PacketError

from pyramid.events import NewRequest

import slapdtest
import sqlalchemy as sa
import transaction
from webtest import TestApp as TestApp_

from pyramid_helpers import main
from pyramid_helpers.forms import State
from pyramid_helpers.models import Base
from pyramid_helpers.models import DBSession
from pyramid_helpers.models.core import User
from pyramid_helpers.scripts.initializedb import add_articles
from pyramid_helpers.scripts.initializedb import add_users


SETTINGS = {
    'use': 'egg:pyramid-helpers',

    # Auth
    'auth.enabled': 'true',
    'auth': {
        'auth': {
            'backend': 'database',
            'policies': 'all',
            'get_principals': 'pyramid_helpers.models.core.get_principals',
            'get_user_by_username': 'pyramid_helpers.models.core.get_user_by_username',
        },

        'policy:basic': {
            'realm': 'Pyramid-Helpers Application',
        },

        'policy:cookie': {
            'secret': 'the-big-secret-for-secured-authentication',
            'hashalg': 'sha512',
        },

        'policy:remote': {
            # 'fake_user': 'admin',
            'header': 'X-FORWARDED-USER',
            'login_url': 'https://idp.domain.tld/login',
            'logout_url': 'https://idp.domain.tld/logout',
        },

        'policy:token': {
            'header': 'X-PH-Authentication-Token',
            'get_username_by_token': 'pyramid_helpers.models.core.get_username_by_token',
        },
    },

    # Forms
    'forms.enabled': 'true',

    # I18n
    'i18n.enabled': 'true',
    'i18n.available_languages': 'fr en',
    'i18n.default_locale_name': 'en',
    'i18n.directories': 'pyramid_helpers:locale formencode:i18n',
    'i18n.domain': 'pyramid-helpers',

    # LDAP
    'ldap.enabled': 'true',
    'ldap': {
        'ldap': {
            'uri': None,                                                        # Will be set from `slapd` fixture
            'tls': 'true',
            'ca_certs': None,                                                   # Will be set from `slapd` fixture
            'certfile': None,                                                   # Will be set from `slapd` fixture
            'keyfile': None,                                                    # Will be set from `slapd` fixture
            'auth_base_dn': None,                                               # Will be set from `slapd` fixture
            'auth_filter': '(&(cn={0})(objectClass=simpleSecurityObject))',
            'base_dn': None,                                                    # Will be set from `slapd` fixture
            'bind_dn': None,                                                    # Will be set from `slapd` fixture
            'bind_credential': 'what-is-good-for-security',
            'retry_after': 5,
        },
    },

    # Pagers
    'pagers.enabled': 'true',
    'pagers.limit': '10 20 30 50',

    # Predicates
    'predicates.enabled': 'true',

    # Radius
    'radius.enabled': 'true',
    'radius': {
        'server:1': {
            'server': '127.0.0.1',
            'secret': 'secret123',
        },
        'attr:1': {
            'name': 'username',
            'type': 'string',
        },
        'attr:2': {
            'name': 'password',
            'type': 'string',
        },
    },

    # Renderers
    'renderers.csv.enabled': 'true',
    'renderers.json.enabled': 'true',
    'renderers.json.callback': 'callback',
    'renderers.json.indent': '4',

    # Default timezone
    'timezone': 'Europe/Paris',

    # API-Doc
    'api_doc.bootstrap_version': '3',

    'api_doc.libraries.handlebars': 'pyramid_helpers:static/lib/handlebars-4.0.12.min.js',
    'api_doc.libraries.moment': '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js',
    'api_doc.libraries.bootstrap-datetimepicker': '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js //cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css',
    'api_doc.libraries.bootstrap-touchspin': '//cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/4.3.0/jquery.bootstrap-touchspin.min.js //cdnjs.cloudflare.com/ajax/libs/bootstrap-touchspin/4.3.0/jquery.bootstrap-touchspin.min.css',
    'api_doc.libraries.font-awesome': '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/fontawesome.min.css //cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js //cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css //cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js',

    # Third party libraries
    'mako.directories': 'pyramid_helpers:templates',
    'sqlalchemy.url': 'sqlite:///',
}


USERS = {
    'admin': {
        'groups': ['admin', ],
        'password': 'admin',
    },  # Timezone will be set to Europe/Paris
    'phelpers': {
        'groups': ['system', ],
        'password': 'what-is-good-for-security',
    },
    'user1': {
        'groups': ['guest', ],
        'firstname': 'John',
        'lastname': 'Doe',
        'password': 'user1',
        'timezone': 'UTC',
    },  # Hum... UTC
    'user2': {
        'groups': ['guest', ],
        'firstname': 'Jane',
        'lastname': 'Doe',
        'password': 'user2',
        'timezone': 'Indian/Reunion',
    },  # UTC+4
    'user3': {
        'groups': ['guest', ],
        'password': 'user3',
        'timezone': 'Invalid',
    },  # Timezone will be set to Europe/Paris
}


# pylint: disable=redefined-outer-name,no-member


class RadiusServer(Server, Thread):
    """ Fake RADIUS server """

    def __init__(self, **kwargs):
        """ Initialization """

        # Thread initialization
        Thread.__init__(self)

        self.daemon = True
        self.loop = True

        # Server initialization
        self._poll = None
        self._fdmap = None

        Server.__init__(self, **kwargs)

    def HandleAuthPacket(self, pkt):
        """ Handle authentication packet """

        if 'username' not in pkt or 'password' not in pkt:
            # Ignore invalid packet
            return

        # Get username
        username = pkt['username'][0]

        # Decrypt password
        password = pkt.PwDecrypt(pkt['password'][0])

        # Prepare reply
        reply = self.CreateReplyPacket(pkt)

        # Check credentials
        if username in USERS and USERS[username]['password'] == password:
            reply.code = AccessAccept
        else:
            reply.code = AccessReject

        self.SendReplyPacket(pkt.fd, reply)

    def run(self):
        """
        Main loop.
        This method is the main loop for a RADIUS server. It waits
        for packets to arrive via the network and calls other methods
        to process them.
        """

        self._poll = select.poll()
        self._fdmap = {}
        self._PrepareSockets()

        while self.loop:
            for (fd, event) in self._poll.poll():
                if event != select.POLLIN:
                    continue

                try:
                    fdo = self._fdmap[fd]
                    self._ProcessInput(fdo)
                except (ServerPacketError, PacketError):
                    pass

    def stop(self):
        """ Stop the server """

        self.loop = False


class TestRoute:
    """ Test route for Pyramid-Helpers """

    name = 'index'


class TestApp(TestApp_):
    """ Test application for Pyramid-Helpers """

    def __init__(self, app, **kwargs):
        # calling inherited
        super().__init__(app, **kwargs)

        # For `route_path()`
        self.__request = app.request_factory.blank('/')
        self.__request.registry = app.registry

    def get_request(self, route_name, *elements, data=None, method='GET', **kw):
        """ Get a request object """

        path = self.route_path(route_name, *elements, **kw)

        request = self.app.request_factory.blank(path, POST=data)
        request.headers['Accept-Language'] = 'en'
        request.matched_route = TestRoute()
        request.method = method
        request.registry = self.app.registry

        # Setup the request
        self.app.registry.notify(NewRequest(request))

        return request

    def route_path(self, route_name, *elements, **kw):
        """ Wrapper to `Request.route_path()` """

        return self.__request.route_path(route_name, *elements, **kw)

    def sign_in(self, username, password, redirect='/'):
        """ Sign-in to application """

        # Perform sign-in
        params = {
            'username': username,
            'password': password,
            'redirect': redirect,
        }

        return self.post(self.route_path('auth.sign-in'), params=params)

    def sign_out(self):
        """ Sign-out from application """

        return self.get(self.route_path('auth.sign-out'))


@pytest.fixture(scope='session')
def wsgi_app(slapd):
    """ Initialize Pyramid-Helper application """

    settings = deepcopy(SETTINGS)

    # Update LDAP settings
    settings['ldap']['ldap'].update({
        'uri': slapd.ldap_uri,
        'ca_certs': slapd.cafile,
        'certfile': slapd.clientcert,
        'keyfile': slapd.clientkey,
        'auth_base_dn': slapd.suffix,
        'base_dn': slapd.suffix,
        'bind_dn': f'cn=phelpers,ou=users,{slapd.suffix}',
    })

    # Create WSGI application
    app = main({}, **settings)

    # Initialize the database
    Base.metadata.create_all(bind=DBSession.get_bind())

    # Populate table users
    with transaction.manager:
        add_users(USERS)

    # Populate table articles
    with transaction.manager:
        add_articles()

    # Populate LDAP
    with transaction.manager:
        groups = set()
        for user in DBSession.execute(sa.select(User).order_by(User.username)).scalars():
            # Add user
            slapd.ldapadd((
                f'dn: cn={user.username},ou=users,{slapd.suffix}\n'
                'objectClass: simpleSecurityObject\n'
                'objectClass: organizationalRole\n'
                f'cn: {user.fullname}\n'
                f'userPassword: {user.password}\n'
                '\n'
            ))
            for group in user.groups:
                if group.name in groups:
                    # Add user to existing group
                    slapd.ldapmodify((
                        f'dn: cn={group.name},ou=groups,{slapd.suffix}\n'
                        'changeType: modify\n'
                        'add: member\n'
                        f'member: cn={user.username},ou=users,{slapd.suffix}\n'
                        '\n'
                    ))
                else:
                    # Add group
                    slapd.ldapadd((
                        f'dn: cn={group.name},ou=groups,{slapd.suffix}\n'
                        'objectClass: groupOfNames\n'
                        'objectClass: top\n'
                        f'member: cn={user.username},ou=users,{slapd.suffix}\n'
                        f'cn: {group.name}\n'
                        '\n'
                    ))
                    groups.add(group.name)

    return app


@pytest.fixture(scope='session')
def app(wsgi_app):
    """ Initialize main session (anonymous) """

    yield TestApp(wsgi_app)


@pytest.fixture(scope='session')
def app_a(wsgi_app):
    """ Initialize a session for user admin """

    app = TestApp(wsgi_app)

    app.sign_in('admin', 'admin')

    yield app

    app.sign_out()


@pytest.fixture(scope='session')
def app_1(wsgi_app):
    """ Initialize a session for user user1 """

    app = TestApp(wsgi_app)

    app.sign_in('user1', 'user1')

    yield app

    app.sign_out()


@pytest.fixture(scope='session')
def app_2(wsgi_app):
    """ Initialize a session for user user2 """

    app = TestApp(wsgi_app)

    app.sign_in('user2', 'user2')

    yield app

    app.sign_out()


@pytest.fixture(scope='session')
def app_3(wsgi_app):
    """ Initialize a session for user user3 """

    app = TestApp(wsgi_app)

    app.sign_in('user3', 'user3')

    yield app

    app.sign_out()


@pytest.fixture(scope='session')
def radius_server():
    """ Launch a fake RADIUS server """

    dictionary = Dictionary(
        StringIO(
            'ATTRIBUTE username 1 string\n'
            'ATTRIBUTE password 2 string\n'
        )
    )

    server = RadiusServer(dict=dictionary)
    server.hosts['127.0.0.1'] = RemoteHost('127.0.0.1', b'secret123', 'localhost')

    try:
        server.BindToAddress('127.0.0.1')
        server.start()

        yield server

    finally:
        server.stop()


@pytest.fixture(scope='session')
def slapd():
    """ Launch a fake LDAP server """

    slapd = slapdtest.SlapdObject()

    slapd.slapd_conf_template += (
        'olcAccess: to *\n'
        '    by * write\n'
    )

    suffix = explode_dn(slapd.suffix)
    suffix_dc = suffix[0].split('=')[1]

    try:
        slapd.start()

        slapd.ldapadd((
            f'dn: {slapd.suffix}\n'
            'objectClass: dcObject\n'
            'objectClass: organization\n'
            f'dc: {suffix_dc}\n'
            f'o: {suffix_dc}\n'
            '\n'
            f'dn: ou=users,{slapd.suffix}\n'
            'objectClass: organizationalUnit\n'
            'ou: users\n'
            '\n'
            f'dn: ou=groups,{slapd.suffix}\n'
            'objectClass: organizationalUnit\n'
            'ou: groups\n'
            '\n'
            f'dn: {slapd.root_dn}\n'
            'objectClass: applicationProcess\n'
            f'cn: {slapd.root_cn}\n'
            '\n'
        ))

        yield slapd

    finally:
        slapd.stop()


@pytest.fixture(scope='session')
def state(app):
    """ Initialize main session (anonymous) """

    yield State(app.get_request('validators'))
