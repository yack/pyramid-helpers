""" Test functions for forms.validators module """

import datetime

import formencode
from formencode import validators

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.forms.validators import Color
from pyramid_helpers.forms.validators import DateTime
from pyramid_helpers.forms.validators import List
from pyramid_helpers.forms.validators import Month
from pyramid_helpers.forms.validators import Time
from pyramid_helpers.forms.validators import Week
from pyramid_helpers.utils import get_tzinfo


# pylint: disable=no-member


@test_steps('to_hex', 'from_hex', 'from_int', 'invalid', 'empty')
def test_color_hex(state):
    """ Test function for forms.validators.Color with hex strings """

    color_hex = '#aabbcc'
    color_int = [170, 187, 204]

    validator = Color(format='hex')

    expect(validator.to_python(color_hex, state=state)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_hex, state=state)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_int, state=state)).to.eq(color_hex)
    yield

    expect(lambda: validator.to_python('invalid', state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield


@test_steps('to_hex', 'from_hex', 'from_int', 'invalid', 'empty')
def test_color_int(state):
    """ Test function for forms.validators.Color with integers """

    color_hex = '#aabbcc'
    color_int = [170, 187, 204]

    validator = Color(format='int')

    expect(validator.to_python(color_hex, state=state)).to.eq(color_int)
    yield

    expect(validator.from_python(color_hex, state=state)).to.eq(color_hex)
    yield

    expect(validator.from_python(color_int, state=state)).to.eq(color_hex)
    yield

    expect(lambda: validator.to_python('invalid', state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield


@test_steps('to', 'from', 'to_invalid', 'from_invalid', 'empty')
def test_date(state):
    """ Test function for forms.validators.DateTime with date objects """

    value_str = '12/25/2020'
    value_inv = '25/12/2020'
    value_obj = datetime.date(2020, 12, 25)

    validator = DateTime(format='%m/%d/%Y', is_date=True, is_localized=False)

    expect(validator.to_python(value_str, state=state)).to.eq(value_obj)
    yield

    expect(validator.from_python(value_obj, state=state)).to.eq(value_str)
    yield

    expect(lambda: validator.to_python(value_inv, state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.from_python(value_inv, state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield


@test_steps('to', 'to_datetime', 'from', 'to_invalid', 'from_invalid', 'to_year_min', 'from_year_min', 'empty')
def test_datetime(state):
    """ Test function for forms.validators.DateTime with datetime objects """

    tzinfo = get_tzinfo(state.request)

    value_str = '12/25/2020 12:52:32'
    value_inv = '12/25/2020 25:52:32'
    value_obj = datetime.datetime(2020, 12, 25, 12, 52, 32, tzinfo=tzinfo)

    validator = DateTime(format='%m/%d/%Y %H:%M:%S', is_date=False, is_localized=False)

    expect(validator.to_python(value_obj, state=state)).to.eq(value_obj)
    yield

    expect(validator.to_python(value_str, state=state)).to.eq(value_obj)
    yield

    expect(validator.from_python(value_obj, state=state)).to.eq(value_str)
    yield

    expect(lambda: validator.to_python(value_inv, state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.from_python(value_inv, state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('01/01/1852 00:00:00', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.from_python(datetime.datetime(1852, 1, 1), state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield


@test_steps('int_to', 'int_from', 'int_invalid', 'str_to', 'str_to2', 'str_from', 'empty', 'invalid_to', 'invalid_from')
def test_list(state):
    """ Test function for forms.validators.List """

    class Invalid:
        """ Invalid string class """

        def __str__(self):
            """ Prevent `object.__str__()` from being called """

            raise AttributeError('No such attribute')

    # List of integers
    validator = List(validators.Int())

    expect(validator.to_python('1, 2, 3', state=state)).to.eq([1, 2, 3])
    yield

    expect(validator.from_python([1, 2, 3], state=state)).to.eq('1,2,3')
    yield

    expect(lambda: validator.to_python('::', state=state)).to.throw(formencode.Invalid)
    yield

    # List of strings
    validator = List()

    expect(validator.to_python('a, b, c', state=state)).to.eq(['a', 'b', 'c'])
    yield

    expect(validator.to_python('::', state=state)).to.eq(['::'])
    yield

    expect(validator.from_python(['a', 'b', 'c'], state=state)).to.eq('a,b,c')
    yield

    expect(validator.to_python(None, state=state)).to.eq([])
    yield

    # invalid_to
    expect(lambda: validator.to_python(Invalid(), state=state)).to.throw(formencode.Invalid)
    yield

    # invalid_from
    expect(lambda: validator.from_python([Invalid()], state=state)).to.throw(formencode.Invalid)
    yield


@test_steps('valid_to', 'valid_from', 'year_min', 'month_min', 'month_max', 'empty', 'invalid')
def test_month(state):
    """ Test function for forms.validators.Month """

    validator = Month()

    expect(validator.to_python('1900-01', state=state)).to.eq((1900, 1))
    expect(validator.to_python('2020-01', state=state)).to.eq((2020, 1))
    expect(validator.to_python('2020-12', state=state)).to.eq((2020, 12))
    yield

    expect(validator.from_python((1900, 1), state=state)).to.eq('1900-01')
    expect(validator.from_python((2020, 1), state=state)).to.eq('2020-01')
    expect(validator.from_python((2020, 12), state=state)).to.eq('2020-12')
    yield

    expect(lambda: validator.to_python('1899-01', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-00', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-13', state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield

    expect(lambda: validator.to_python('invalid', state=state)).to.throw(formencode.Invalid)
    yield


@test_steps('valid_to', 'valid_from', 'hours_min', 'hours_max', 'minutes_min', 'minutes_max', 'empty')
def test_time(state):
    """ Test function for forms.validators.Time """

    validator = Time()

    expect(validator.to_python('00:00', state=state)).to.eq((0, 0))
    expect(validator.to_python('11:11', state=state)).to.eq((11, 11))
    expect(validator.to_python('23:59', state=state)).to.eq((23, 59))
    yield

    expect(validator.from_python((0, 0), state=state)).to.eq('00:00')
    expect(validator.from_python((11, 11), state=state)).to.eq('11:11')
    expect(validator.from_python((23, 59), state=state)).to.eq('23:59')
    yield

    expect(lambda: validator.to_python('-1:00', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('24:00', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('00:-1', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('00:60', state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield


@test_steps('valid_to', 'valid_from', 'year_min', 'week_min', 'week_max', 'empty', 'invalid')
def test_week(state):
    """ Test function for forms.validators.Week """

    validator = Week()

    expect(validator.to_python('1900-W01', state=state)).to.eq((1900, 1))
    expect(validator.to_python('2020-W01', state=state)).to.eq((2020, 1))
    expect(validator.to_python('2020-W52', state=state)).to.eq((2020, 52))
    yield

    expect(validator.from_python((1900, 1), state=state)).to.eq('1900-W01')
    expect(validator.from_python((2020, 1), state=state)).to.eq('2020-W01')
    expect(validator.from_python((2020, 52), state=state)).to.eq('2020-W52')
    yield

    expect(lambda: validator.to_python('1899-W01', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-W00', state=state)).to.throw(formencode.Invalid)
    yield

    expect(lambda: validator.to_python('2020-W53', state=state)).to.throw(formencode.Invalid)
    yield

    expect(validator.to_python(None, state=state)).to.be.none()
    yield

    expect(lambda: validator.to_python('invalid', state=state)).to.throw(formencode.Invalid)
    yield
