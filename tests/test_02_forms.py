""" Test functions for forms module """

from pytest_steps import test_steps
from robber import expect

from formencode.validators import Int
from formencode.validators import String
from formencode import ForEach
from formencode import Schema

from pyramid_helpers.forms import Form
from pyramid_helpers.forms import State
from pyramid_helpers.forms import validate


# pylint: disable=no-member


class DummySchema(Schema):
    """ Dummy formencode schema """

    allow_extra_fields = True
    filter_extra_fields = True

    value = Int(not_empty=True)


class SimpleSchema(Schema):
    """ Simple formencode schema """

    allow_extra_fields = True
    filter_extra_fields = True

    value1 = String()
    value2 = String()


class ComplexSchema(Schema):
    """ Complex formencode schema """

    simple = SimpleSchema()
    value1 = String()
    value2 = String()
    values = ForEach(String())


def get_form(request, schema, **kwargs):
    """ Get and validate form """

    state = State(request)
    form = Form('form', schema, state, **kwargs)
    form.validate()
    return form


@test_steps('translate', 'pluralize-0', 'pluralize-1', 'pluralize-2')
def test_state(app):
    """ Test function for `State` class """

    request = app.get_request('index')
    state = State(request)

    # translate
    expect(state.translate('Missing value')).to.eq('Missing value')
    yield

    # pluralize
    for index, value in enumerate(('plural', 'singular', 'plural')):
        expect(state.pluralize('singular', 'plural', index)).to.eq(value)
        yield


@test_steps('from_python', 'validate')
def test_validate(app):
    """ Test function for data validation """

    data = {
        'value1': 'value-1',
        'value2': 'value-2',
        'simple': {
            'value1': 'simple-value-1',
            'value2': 'simple-value-2',
        },
        'values': [
            f'values-{index}'
            for index in range(3)
        ],
    }

    request = app.get_request('index')
    state = State(request)
    form = Form('form', ComplexSchema, state)

    # from_python
    form.from_python(data)

    expect(form.decoded).to.eq(data)
    expect(form.errors).to.eq({})
    expect(form.result).to.eq({})
    yield

    # validate
    form.from_python(data, validate=True)

    expect(form.decoded).to.eq(data)
    expect(form.result).to.eq(data)
    yield


@test_steps('disabled', 'enabled', 'missing', 'invalid', 'ok')
def test_csrf_protection(app):
    """ Test function for CSRF protection """

    data = {
        'value1': 'value-1',
        'value2': 'value-2',
    }
    request = app.get_request('index', data=data, method='POST')

    # disabled
    form = get_form(request, SimpleSchema)
    expect(form.csrf_token).to.be.none()
    yield

    # enabled
    form = get_form(request, SimpleSchema, csrf_protect=True)
    expect(form.csrf_token).to.eq(request.session.get_csrf_token())
    yield

    # missing
    expect(len(form.result)).to.eq(0)
    expect(form.errors).to.contain('_csrf_token')
    expect(form.errors['_csrf_token']).to.eq('Invalid CSRF token')
    yield

    # invalid
    data = {
        'value1': 'value-1',
        'value2': 'value-2',
        '_csrf_token': 'invalid',
    }
    request = app.get_request('index', data=data, method='POST')
    form = get_form(request, SimpleSchema, csrf_protect=True)

    expect(len(form.result)).to.eq(0)
    expect(form.errors).to.contain('_csrf_token')
    expect(form.errors['_csrf_token']).to.eq('Invalid CSRF token')
    yield

    # ok
    data = {
        'value1': 'value-1',
        'value2': 'value-2',
    }
    request = app.get_request('index', data=data, method='POST')

    # Add _csrf_token once the request is setup
    request.POST['_csrf_token'] = request.session.get_csrf_token()

    form = get_form(request, SimpleSchema, csrf_protect=True)

    expect(form.errors).to.eq({})
    expect(form.result).to.contain('value1')
    expect(form.result['value1']).to.eq('value-1')
    expect(form.result).to.contain('value2')
    expect(form.result['value2']).to.eq('value-2')
    yield


@test_steps('missing', 'value1')
def test_error(app):
    """ Test function for error getter """

    # missing
    request = app.get_request('index', data={'value': 42}, method='POST')
    form = get_form(request, DummySchema)

    expect(form.error('value')).to.be.none()
    yield

    # value1
    request = app.get_request('index', method='POST')
    form = get_form(request, DummySchema)

    expect(form.error('value')).to.eq('Missing value')
    yield


@test_steps('value1', 'simple.value1', 'values-0', 'missing', 'invalid')
def test_value(app):
    """ Test function for value getter """

    data = {
        'value1': 'value-1',
        'value2': 'value-2',
        'simple': {
            'value1': 'simple-value-1',
            'value2': 'simple-value-2',
        },
        'values': [
            f'values-{index}'
            for index in range(3)
        ],
    }

    request = app.get_request('index')
    state = State(request)
    form = Form('form', ComplexSchema, state)

    form.from_python(data, validate=True)

    # value1
    expect(form.value('value1')).to.eq('value-1')
    expect(form.value('value2')).to.eq('value-2')
    yield

    # simple.value1
    expect(form.value('simple.value1')).to.eq('simple-value-1')
    expect(form.value('simple.value2')).to.eq('simple-value-2')
    yield

    # values-0
    for index in range(3):
        expect(form.value(f'values-{index}')).to.eq(f'values-{index}')
    yield

    # missing
    expect(form.value('')).to.eq('')
    yield

    # invalid
    expect(form.value('invalid')).to.eq('')
    expect(form.value('values-4')).to.eq('')
    yield


@test_steps('no_args', 'no_request', 'request_ok')
def test_wrapper(app):
    """ Test function for validation wrapper """

    @validate('form', DummySchema)
    def dummy_view(*_):
        """ Dummy view """

    # No args
    # pylint: disable=unnecessary-lambda
    expect(lambda: dummy_view()).to.throw(ValueError)
    yield

    # No request
    expect(lambda: dummy_view('foo', 'bar')).to.throw(ValueError)
    yield

    # Request OK
    request = app.get_request('index')
    dummy_view(request)

    expect(request.forms).to.contain('form')
    yield


@test_steps('get', 'post', 'json_error', 'json_ok', 'merge_error', 'merge_ok', 'invalid_extract', 'invalid_method')
def test_extract(app):
    """ Test function for data extraction """

    # get
    request = app.get_request('index', data={'value': 2}, _query={'value': 1})
    form = get_form(request, DummySchema, method='get', extract='get')

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(1)
    yield

    # post
    request = app.get_request('index', data={'value': 2}, _query={'value': 1}, method='POST')
    form = get_form(request, DummySchema, method='post', extract='post')

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(2)
    yield

    # json error
    request = app.get_request('index', method='POST')
    form = get_form(request, DummySchema, extract='json')

    expect(form.errors).to.contain('_content')
    expect(form.errors['_content']).to.eq('Invalid JSON content')
    yield

    # json ok
    request = app.get_request('index', method='POST')
    request.body = b'{"value": 42}'
    form = get_form(request, DummySchema, extract='json')

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    yield

    # merge error
    request = app.get_request('index', data={'value': 2}, _query={'value': 1}, method='POST')
    form = get_form(request, DummySchema, method='post')

    expect(form.errors).to.contain('value')
    expect(form.errors['value']).to.eq('Please enter an integer value')
    yield

    # merge ok
    request = app.get_request('index', _query={'value': 1}, method='POST')
    form = get_form(request, DummySchema, method='post')

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(1)
    yield

    # invalid extract
    request = app.get_request('index')

    expect(lambda: get_form(request, DummySchema, extract='invalid')).to.throw(ValueError)
    yield

    # invalid method
    request = app.get_request('index')

    expect(lambda: get_form(request, DummySchema, method='invalid')).to.throw(ValueError)
    yield


@test_steps('get_only', 'get_save', 'get_load', 'from_python')
def test_persistence(app):
    """ Test function for data persistence """

    # get_only
    request = app.get_request('index', _query={'value': 42})

    expect(lambda: get_form(request, DummySchema, method='post', persistent=True)).to.throw(ValueError)
    yield

    # get save
    request = app.get_request('index', _query={'value': 42})
    session = request.session
    form = get_form(request, DummySchema, method='get', persistent=True)

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    yield

    # get load
    request = app.get_request('index')
    request.session = session
    form = get_form(request, DummySchema, method='get', persistent=True)

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    yield

    # from_python
    request = app.get_request('index')
    form = get_form(request, DummySchema, method='get', persistent=True)

    form.from_python({'value': 42}, validate=True)

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    expect(request.GET).to.contain('value')
    expect(request.GET['value']).to.eq('42')
    yield


@test_steps('missing', 'invalid', 'ok')
def test_get(app):
    """ Test function for GET method """

    # missing
    request = app.get_request('index')
    form = get_form(request, DummySchema, method='get')

    expect(form.errors).to.contain('value')
    expect(form.errors['value']).to.eq('Missing value')
    yield

    # invalid
    request = app.get_request('index', _query={'value': 'foobar'})
    form = get_form(request, DummySchema, method='get')

    expect(form.errors).to.contain('value')
    expect(form.errors['value']).to.eq('Please enter an integer value')
    yield

    # ok
    request = app.get_request('index', _query={'value': 42})
    form = get_form(request, DummySchema, method='get')

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    yield


@test_steps('get', 'missing', 'invalid', 'ok')
def test_post(app):
    """ Test function for POST method """

    # get
    request = app.get_request('index')
    form = get_form(request, DummySchema)

    expect(form.errors).to.eq({})
    expect(form.result).to.eq({})
    yield

    # missing
    request = app.get_request('index', method='POST')
    form = get_form(request, DummySchema)

    expect(form.errors).to.contain('value')
    expect(form.errors['value']).to.eq('Missing value')
    yield

    # invalid
    request = app.get_request('index', data={'value': 'foobar'}, method='POST')
    form = get_form(request, DummySchema)

    expect(form.errors).to.contain('value')
    expect(form.errors['value']).to.eq('Please enter an integer value')
    yield

    # ok
    request = app.get_request('index', data={'value': 42}, method='POST')
    form = get_form(request, DummySchema)

    expect(form.result).to.contain('value')
    expect(form.result['value']).to.eq(42)
    yield
