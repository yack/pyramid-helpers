""" Test functions for utils module """

import os
from tempfile import NamedTemporaryFile
from tempfile import gettempdir

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.utils import deprecated
from pyramid_helpers.utils import get_settings
from pyramid_helpers.utils import get_tzinfo
from pyramid_helpers.utils import request_cache
from pyramid_helpers.utils import resolve_dotted
from pyramid_helpers.utils import TIMEZONE

# pylint: disable=no-member


@test_steps('deprecated')
def test_deprecated(recwarn):
    """ Test function for utils.deprecated()"""

    def new_func():
        return True

    result = deprecated('old_func', new_func)()
    expect(result).to.be.true()

    warning = recwarn.pop(DeprecationWarning)
    expect(str(warning.message)).to.eq('`old_func()` is deprecated and will be removed soon; use `new_func()')
    yield


@test_steps('missing_key', 'missing_file', 'invalid_mode', 'empty', 'invalid', 'valid', 'section')
def test_settings(wsgi_app):
    """ Test function for utils.get_settings() """

    registry = wsgi_app.registry
    settings = registry.settings

    # Missing key
    expect(get_settings(wsgi_app, 'missing')).to.be.none()
    yield

    settings['test.filepath'] = '/tmp/invalid.ini'

    # Missing file
    expect(get_settings(wsgi_app, 'test')).to.be.none()
    yield

    # Invalid mode
    with NamedTemporaryFile(mode='wt', encoding='utf-8') as fp:
        settings.pop('test', None)
        settings['test.filepath'] = fp.name

        os.chmod(fp.name, 0)

        if os.getuid() == 0:
            expect(get_settings(wsgi_app, 'test')).to.eq({})
        else:
            expect(get_settings(wsgi_app, 'test')).to.be.none()
        yield

    # Invalid content
    with NamedTemporaryFile(mode='wt', encoding='utf-8') as fp:
        settings.pop('test', None)
        settings['test.filepath'] = fp.name

        fp.write('[section\nkey\n')
        fp.flush()
        fp.seek(0)

        expect(get_settings(wsgi_app, 'test')).to.be.none()
        yield

    # Empty
    with NamedTemporaryFile(mode='wt', encoding='utf-8') as fp:
        settings.pop('test', None)
        settings['test.filepath'] = fp.name

        expect(get_settings(wsgi_app, 'test')).to.eq({})
        yield

    # Valid
    with NamedTemporaryFile(mode='wt', encoding='utf-8') as fp:
        settings.pop('test', None)
        settings['test.filepath'] = fp.name

        fp.write('[section]\nkey = value\n')
        fp.flush()
        fp.seek(0)

        expect(get_settings(wsgi_app, 'test')).to.eq({'section': {'here': gettempdir(), 'key': 'value'}})
        yield

    # Section
    expect(get_settings(wsgi_app, 'test', section='section')).to.eq({'here': gettempdir(), 'key': 'value'})
    yield


@test_steps('from_authenticated_user', 'from_settings', 'from_default')
def test_get_tzinfo(app):
    """ Test function for utils.get_tz_info() """

    registry = app.app.registry
    settings = registry.settings

    class User:
        """ Fake user class """

        timezone = 'Europe/Paris'

    # From authenticated user
    request = app.get_request('index')
    request.authenticated_user = User()

    tzinfo = get_tzinfo(request)

    expect(tzinfo).not_to.be.none()
    expect(tzinfo.key).to.eq('Europe/Paris')
    yield

    # From settings
    request = app.get_request('index')
    tzinfo = get_tzinfo(request)

    expect(tzinfo).not_to.be.none()
    expect(tzinfo.key).to.eq(settings['timezone'])
    yield

    # From default
    settings['timezone'] = 'invalid'

    request = app.get_request('index')
    tzinfo = get_tzinfo(request)

    expect(tzinfo).not_to.be.none()
    expect(tzinfo.key).to.eq(TIMEZONE)
    yield


@test_steps('missing_request', 'request_ok', 'cache_ok')
def test_request_cache(app):
    """ Test function for utils.request_cache() """

    @request_cache()
    def cached_func(request):
        return request.path

    # Missing request
    expect(lambda: cached_func('invalid')).to.throw(ValueError)
    yield

    # Request OK
    request = app.get_request('index')
    expect(cached_func(request)).to.eq('/')
    yield

    # Cache OK
    expect(getattr(cached_func, '__wrapped__', None)).not_to.be.none()
    expect(getattr(cached_func.__wrapped__, 'cache', None)).not_to.be.none()
    yield


@test_steps('empty', 'invalid', 'missing', 'valid')
def test_resolv_dotted():
    """ Test function for utils.resolve_dotted() """

    # Empty
    expect(resolve_dotted(None)).to.be.none()
    yield

    # Invalid
    expect(resolve_dotted('foo.bar')).to.be.none()
    yield

    # Missing
    expect(lambda: resolve_dotted('pyramid_helpers.foo')).to.throw(AttributeError)
    yield

    # Valid
    expect(resolve_dotted('robber.expect')).to.eq(expect)
    yield
