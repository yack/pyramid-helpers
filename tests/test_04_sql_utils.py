""" Test functions for sql_utils module """

from pytest_steps import test_steps
from robber import expect

import sqlalchemy as sa
from sqlalchemy.orm import aliased

from pyramid_helpers.models.articles import Article
from pyramid_helpers.models.core import Group
from pyramid_helpers.models.core import User
from pyramid_helpers.sql_utils import get_entity
from pyramid_helpers.sql_utils import get_mapper

# pylint: disable=no-member


@test_steps('invalid', 'article', 'group', 'user')
def test_mapper():
    """ Test function for sql_utils.get_mapper() """

    class Empty:
        """ Empty class """

    # invalid
    expect(get_mapper(Empty)).to.be.none()
    yield

    for model in (Article, Group, User):
        expect(get_mapper(model.__table__).entity).to.equal(model)
        yield


@test_steps('article', 'article_invalid', 'aliased', 'aliased_invalid', 'join', 'join_aliased', 'join_invalid', 'select', 'select_aliased', 'select_invalid')
def test_get_entity():
    """ Test function for sql_utils.get_entity() """

    article_a = aliased(Article, name='article_a')
    user_a = aliased(User, name='user_a')

    # article
    select = sa.select(Article)
    expect(get_entity(select, 'article')).to.equal(Article)
    yield

    # article_invalid
    select = sa.select(Article)
    expect(get_entity(select, 'invalid')).to.be.none()
    yield

    # aliased
    select = sa.select(article_a)
    expect(get_entity(select, 'article_a')).to.equal(article_a)
    yield

    # aliased_invalid
    select = sa.select(article_a)
    expect(get_entity(select, 'article')).to.equal(Article)
    yield

    # join
    select = sa.select(Article).join(User, Article.author)
    expect(get_entity(select, 'article')).to.equal(Article)
    expect(get_entity(select, 'user')).to.equal(User)
    yield

    # join_aliased
    select = sa.select(article_a).join(user_a, article_a.author)
    expect(get_entity(select, 'article_a')).to.equal(article_a)
    expect(get_entity(select, 'user_a')).to.equal(user_a)
    yield

    # join invalid
    select = sa.select(article_a).join(user_a, article_a.author)
    expect(get_entity(select, 'article')).to.equal(Article)
    expect(get_entity(select, 'user')).to.equal(User)
    yield

    # select
    select = sa.select(Article).join(User, Article.author)
    select = select.where(
        sa.and_(
            Article.title.is_(None),
            User.username.is_(None),
        )
    )
    expect(str(select)).to.eq(
        'SELECT articles.id, articles.author_id, articles.creation_date, articles.modification_date, articles.title, articles.text, articles.status \n'
        'FROM articles JOIN users ON users.id = articles.author_id \n'
        'WHERE articles.title IS NULL AND users.username IS NULL'
    )
    yield

    # select_aliased
    select = sa.select(article_a).join(user_a, article_a.author)
    select = select.where(
        sa.and_(
            get_entity(select, 'article_a').title.is_(None),
            get_entity(select, 'user_a').username.is_(None),
        )
    )
    expect(str(select)).to.eq(
        'SELECT article_a.id, article_a.author_id, article_a.creation_date, article_a.modification_date, article_a.title, article_a.text, article_a.status \n'
        'FROM articles AS article_a JOIN users AS user_a ON user_a.id = article_a.author_id \n'
        'WHERE article_a.title IS NULL AND user_a.username IS NULL'
    )
    yield

    # select_invalid
    select = sa.select(article_a).join(user_a, article_a.author)
    select = select.where(
        sa.and_(
            Article.title.is_(None),
            User.username.is_(None),
        )
    )
    expect(str(select)).to.eq(
        'SELECT article_a.id, article_a.author_id, article_a.creation_date, article_a.modification_date, article_a.title, article_a.text, article_a.status \n'
        'FROM articles AS article_a JOIN users AS user_a ON user_a.id = article_a.author_id, articles, users \n'
        'WHERE articles.title IS NULL AND users.username IS NULL'
    )
    yield
