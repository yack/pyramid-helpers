""" Test functions for auth module """

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.auth import AuthenticationBackend
from pyramid_helpers.auth import check_credentials


# pylint: disable=no-member


class MissingModeAuthenticationBackend(AuthenticationBackend):
    """ Authentication backend with missing name """

    # pylint: disable=abstract-method
    __name__ = None


class InvalidModeAuthenticationBackend(AuthenticationBackend):
    """ Authentication backend with invalid name """

    # pylint: disable=abstract-method
    __name__ = 'foobar'


@test_steps('missing_mode', 'invalid_mode', 'validate_password_not_overridden', 'admin_ok', 'bad_password', 'bad_username')
def test_auth(app):
    """ Test function for authentication backends """

    request = app.get_request('index')

    # Missing mode
    backend = MissingModeAuthenticationBackend()

    expect(backend.setup()).to.be.false()
    yield

    # Invalid mode
    backend = InvalidModeAuthenticationBackend()

    expect(backend.setup()).to.be.true()
    expect(backend.setup()).to.be.false()
    yield

    # validate_password_not_overridden
    expect(lambda: backend.validate_password(request, None, None)).to.throw(NotImplementedError)
    yield

    # Admin OK
    expect(check_credentials(request, 'admin', 'admin')).to.be.true()
    yield

    # Bad password
    expect(check_credentials(request, 'admin', 'bad')).to.be.false()
    yield

    # Bad username
    expect(check_credentials(request, 'bad', 'admin')).to.be.false()
    yield
