""" Test functions for ldap module """

from copy import deepcopy
import time

import ldap

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.ldap import client


# pylint: disable=no-member,unused-argument


@test_steps('with_error', 'admin_ok', 'bad_username', 'bad_password', 'bind_force')
def test_bind(app, slapd):
    """ Test function for LDAP binding """

    dn = f'cn=admin,ou=users,{slapd.suffix}'

    # With error
    expect(lambda: client.bind(dn, 'foo')).to.throw(ldap.LDAPError)
    yield

    # Admin OK
    with client:
        expect(client.bind(dn, 'admin')).to.be.true()
    yield

    # Bad username
    with client:
        expect(client.bind('bad', 'admin')).to.be.false()
    yield

    # Bad password
    with client:
        expect(client.bind(dn, 'bad')).to.be.false()
    yield

    # Bind force
    with client:
        expect(client.bind(dn, 'admin')).to.be.true()
        expect(client.bind(dn, 'admin')).to.be.true()
        expect(client.bind(dn, 'admin', force=True)).to.be.true()
    yield


@test_steps('error', 'wait', 'ok')
def test_wait_until(app, slapd):
    """ Test function for `LDAPClient` wait until feature """

    dn = f'cn=admin,ou=users,{slapd.suffix}'

    # Generate an error
    with client:
        expect(client.fail_count).to.eq(0)
        for count in range(client.max_retry):
            expect(client.fail_count).to.eq(count)
            expect(client.wait_until).to.be.none()
            client.search(filterstr='invalid')

        expect(client.fail_count).to.eq(0)
        expect(client.wait_until).not_to.be.none()
    yield

    # Wait
    with client:
        while time.time() < client.wait_until:
            client.search(dn=dn)
            expect(client.fail_count).to.eq(0)
            expect(client.wait_until).not_to.be.none()
            time.sleep(0.1)
    yield

    # OK
    with client:
        client.search(dn=dn)
        expect(client.fail_count).to.eq(0)
        expect(client.wait_until).to.be.none()
    yield


@test_steps('ok', 'attrlist', 'missing')
def test_get(slapd):
    """ Test function for `LDAPClient.get()` method """

    # OK
    dn = f'cn=admin,ou=users,{slapd.suffix}'
    with client:
        result = client.get(dn)

    expect(result).to.be.a.tuple()
    expect(len(result)).to.eq(2)
    expect(result[0]).to.eq(dn)
    expect(result[1]).to.be.a.dict()
    expect(result[1]).to.contain('objectClass')
    expect(result[1]).to.contain('cn')
    expect(result[1]).to.contain('userPassword')
    yield

    # attrlist
    dn = f'cn=user3,ou=users,{slapd.suffix}'
    with client:
        result = client.get(dn, attrlist=['cn'])

    expect(result).to.be.a.tuple()
    expect(len(result)).to.eq(2)
    expect(result[0]).to.eq(dn)
    expect(result[1]).to.be.a.dict()
    expect(result[1]).to.exclude('objectClass')
    expect(result[1]).to.contain('cn')
    expect(result[1]).to.exclude('userPassword')
    yield

    # Missing
    dn = f'cn=user4,ou=users,{slapd.suffix}'
    with client:
        result = client.get(dn)

    expect(result).to.be.none()
    yield


@test_steps('ok', 'missing', 'invalid')
def test_search(slapd):
    """ Test function for `LDAPClient.search()` method """

    # OK
    dn = f'cn=admin,ou=users,{slapd.suffix}'
    with client:
        result = client.search(dn=dn)

    expect(result).to.be.a.list()
    expect(len(result)).to.eq(1)
    expect(result[0]).to.be.a.tuple()
    expect(len(result[0])).to.eq(2)
    expect(result[0][0]).to.eq(dn)
    expect(result[0][1]).to.be.a.dict()
    expect(result[0][1]).to.contain('objectClass')
    expect(result[0][1]).to.contain('cn')
    expect(result[0][1]).to.contain('userPassword')
    yield

    # Missing
    with client:
        result = client.search(dn=f'cn=missing,ou=users,{slapd.suffix}')

    expect(result).to.be.a.none()
    yield

    # Invalid
    with client:
        result = client.search(dn='invalid')

    expect(result).to.be.a.none()
    expect(client.fail_count).to.ne(0)
    yield


@test_steps('ok')
def test_search_iter(slapd):
    """ Test function for `LDAPClient.search_iter()` method """

    # OK
    result = []
    with client:
        for dn, attrs in client.search_iter(size=1):
            result.append((dn, attrs))

    expect(len(result)).to.ne(0)
    yield


@test_steps('ok', 'already_exists')
def test_create(slapd):
    """ Test function for `LDAPClient.add()` method """

    dn = f'cn=user4,ou=users,{slapd.suffix}'

    attrs = {
        'objectClass': [b'simpleSecurityObject', b'organizationalRole'],
        'cn': [b'user4'],
        'userPassword': [b'clear-pass'],
    }

    # OK
    with client:
        result = client.add(dn, attrs)

    expect(result).to.be.true()
    yield

    # Already exists
    dn = f'cn=user1,ou=users,{slapd.suffix}'

    attrs = {
        'objectClass': [b'simpleSecurityObject', b'organizationalRole'],
        'cn': [b'user1'],
        'userPassword': [b'user1'],
    }

    with client:
        result = client.add(dn, attrs)

    expect(result).to.be.false()
    yield


@test_steps('ok', 'empty', 'missing')
def test_modify(slapd):
    """ Test function for `LDAPClient.modify()` method """

    dn = f'cn=user3,ou=users,{slapd.suffix}'

    # OK
    with client:
        dn, old_attrs = client.get(dn)

        new_attrs = deepcopy(old_attrs)
        new_attrs['userPassword'].append(b'second-one')

        result = client.modify(dn, old_attrs, new_attrs)

    expect(result).to.be.true()
    yield

    # Empty
    with client:
        dn, attrs = client.get(dn)

        result = client.modify(dn, attrs, attrs)

    expect(result).to.be.none()
    yield

    # Missing
    dn = f'cn=missing,ou=users,{slapd.suffix}'

    old_attrs = {
        'objectClass': [b'simpleSecurityObject', b'organizationalRole'],
        'cn': [b'missing'],
        'userPassword': [b'missing'],
    }
    new_attrs = old_attrs.copy()
    new_attrs['userPassword'].append(b'second-one')

    with client:
        result = client.modify(dn, old_attrs, new_attrs)

    expect(result).to.be.false()
    yield


@test_steps('ok', 'missing')
def test_delete(slapd):
    """ Test function for `LDAPClient.delete()` """

    # OK
    dn = f'cn=user2,ou=users,{slapd.suffix}'
    with client:
        result = client.delete(dn)

    expect(result).to.be.true()
    yield

    # Missing
    dn = f'cn=missing,ou=users,{slapd.suffix}'
    with client:
        result = client.delete(dn)

    expect(result).to.be.false()
    yield


@test_steps('admin_ok', 'bad_password', 'bad_username')
def test_auth(app):
    """ Test function for LDAP authentication """

    request = app.get_request('index')

    # Admin OK
    with client:
        result = client.validate_password(request, 'admin', 'admin')

    expect(result).to.be.true()
    yield

    # Bad password
    with client:
        result = client.validate_password(request, 'admin', 'bad')

    expect(result).to.be.false()
    yield

    # Bad username
    with client:
        result = client.validate_password(request, 'bad', 'admin')

    expect(result).to.be.false()
    yield
