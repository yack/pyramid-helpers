""" Test functions for radius module """

from pytest_steps import test_steps
from robber import expect

from pyramid_helpers.radius import client
from pyramid_helpers.radius import RadiusError


# pylint: disable=no-member,unused-argument


@test_steps('with_error', 'admin_ok', 'bad_username', 'bad_password')
def test_auth(app, radius_server):
    """ Test function for RADIUS authentication """

    request = app.get_request('index')

    # With error
    expect(lambda: client.validate_password(request, 'foo', 'bar')).to.throw(RadiusError)
    yield

    # Admin OK
    with client:
        result = client.validate_password(request, 'admin', 'admin')

    expect(result).to.be.true()
    yield

    # Bad password
    with client:
        result = client.validate_password(request, 'admin', 'bad')

    expect(result).to.be.false()
    yield

    # Bad username
    with client:
        result = client.validate_password(request, 'bad', 'admin')

    expect(result).to.be.false()
    yield
