""" Test functions for common views """

import base64

from pytest_steps import test_steps

from robber import expect
import sqlalchemy as sa
from webtest.forms import Upload

from pyramid_helpers.models import DBSession
from pyramid_helpers.models.core import User


# pylint: disable=no-member


@test_steps('index')
def test_index(app):
    """ Test function for index view """

    # Index
    url = app.route_path('index')
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<h1 class="display-5 fw-bold">Welcome to Pyramid Helpers!</h1>')
    yield


@test_steps('anonymous', 'sign_in_page', 'sign_in', 'sign_out', 'error', 'basic', 'remote', 'header', 'token')
def test_sign_in(app):
    """ Test function for sign_in views """

    # Anonymous
    url = app.route_path('articles.search')
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<a class="btn btn-primary me-3" href="/auth/sign-in" title="Sign in">Sign in</a>')
    expect(str(response.html)).to.contain('<h2>Articles</h2>')
    yield

    # Sign-in page
    url = app.route_path('auth.sign-in')
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<form action="/auth/sign-in" method="post" name="signin" role="form">')
    yield

    # Sign-in
    params = {
        'username': 'user1',
        'password': 'user1',
        'redirect': '/foobar',
    }

    url = app.route_path('auth.sign-in')
    response = app.post(url, params=params, status=302)

    expect(response.headers.get('location')).to.contain('http://localhost/foobar')
    yield

    # Sign-out
    url = app.route_path('auth.sign-out')
    response = app.post(url, status=302)

    expect(response.headers.get('location')).to.contain('http://localhost/')
    yield

    # Error
    params = {
        'username': 'user1',
        'password': 'foobar',
        'redirect': '/',
    }

    url = app.route_path('auth.sign-in')
    response = app.post(url, params=params, status=200)

    expect(str(response.html)).to.contain('<form action="/auth/sign-in" method="post" name="signin" role="form">')
    expect(str(response.html)).to.contain('Bad user or password')
    yield

    url = app.route_path('articles.search')

    # Basic
    headers = {'Authorization': 'Basic {0}'.format(base64.b64encode(b'user1:user1').decode('utf-8'))}
    response = app.get(url, headers=headers, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">John Doe</span>')
    yield

    # Remote
    extra_environ = {'REMOTE_USER': 'user1'}
    response = app.get(url, extra_environ=extra_environ, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">John Doe</span>')
    yield

    # Header
    headers = {'X-FORWARDED-USER': 'user1'}
    response = app.get(url, headers=headers, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">John Doe</span>')
    yield

    # Token
    token = DBSession.execute(sa.select(User.token).where(User.username == 'user1').limit(1)).scalar()
    headers = {'X-PH-Authentication-Token': token}
    response = app.get(url, headers=headers, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">John Doe</span>')
    yield


@test_steps(
    'predicate=value1',
    'predicate=value2',
    'predicate=value3 (invalid)',
    'predicate1=1',
    'predicate1=2',
    'predicate1=a (invalid)',
    'predicate1=1, predicate2=3',
    'predicate1=2, predicate2=4',
    'predicate1=a, predicate2=3 (invalid)',
    'predicate1=1, predicate2=b (invalid)',
    'predicate1=a, predicate2=b (invalid)',
)
def test_predicates(app):
    """ Test function for predicates view """

    # predicate=value1
    # predicate=value2
    for value in ('value1', 'value2'):
        url = app.route_path('predicates.enum', predicate=value)
        response = app.get(url, status=200)

        expect(str(response.html)).to.contain(f'<code>predicate={value}</code>')
        yield

    # predicate=value3 (invalid)
    url = app.route_path('predicates.enum', predicate='value3')
    response = app.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield

    # predicate1=1
    # predicate1=2
    for value in (1, 2):
        url = app.route_path('predicates.numeric-1', predicate1=value)
        response = app.get(url, status=200)

        expect(str(response.html)).to.contain(f'<code>predicate1={value}</code>')
        yield

    # predicate1=a (invalid)
    url = app.route_path('predicates.numeric-1', predicate1='a')
    response = app.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield

    # predicate1=1, predicate2=3
    # predicate1=2, predicate2=4
    for value1, value2 in ((1, 3), (2, 4)):
        url = app.route_path('predicates.numeric-2', predicate1=value1, predicate2=value2)
        response = app.get(url, status=200)

        expect(str(response.html)).to.contain(f'<code>predicate1={value1}</code>')
        expect(str(response.html)).to.contain(f'<code>predicate2={value2}</code>')
        yield

    # predicate1=a, predicate2=3 (invalid)
    # predicate1=1, predicate2=b (invalid)
    # predicate1=a, predicate2=b (invalid)
    for value1, value2 in (('a', 3), (1, 'b'), ('a', 'b')):
        url = app.route_path('predicates.numeric-2', predicate1=value1, predicate2=value2)
        response = app.get(url, expect_errors=True)

        expect(response.status_code).to.eq(404)
        yield


@test_steps('en-US', 'fr-FR', 'invalid')
def test_i18n(app):
    """ Test function for i18n """

    url = app.route_path('i18n')

    # en-US
    response = app.get(url, status=200, headers={'Accept-Language': 'en-US, en;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5'})

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain("date_utc = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)          # 2021-01-01 00:00:00+00:00")

    expect(html).to.contain("format_date(date_utc)                                                           # Jan 1, 2021")
    expect(html).to.contain("format_datetime(date_utc)                                                       # Jan 1, 2021 12:00:00 AM")
    expect(html).to.contain("format_date(date_utc, format='long')                                            # January 1, 2021")
    expect(html).to.contain("format_datetime(date_utc, date_format='long')                                   # January 1, 2021 12:00:00 AM")
    expect(html).to.contain("format_datetime(date_utc, date_format='short')                                  # 1/1/21 12:00:00 AM")
    expect(html).to.contain("format_datetime(date_utc, date_format='short', time_format='short')             # 1/1/21 12:00 AM")
    expect(html).to.contain("format_datetime(localtoutc(date_utc), date_format='short', time_format='short') # 1/1/21 12:00 AM")
    expect(html).to.contain("format_datetime(utctolocal(date_utc), date_format='short', time_format='short') # 1/1/21 1:00 AM")
    expect(html).to.contain("format_time(date_utc)                                                           # 12:00:00 AM")
    expect(html).to.contain("format_time(date_utc, format='short')                                           # 12:00 AM")

    expect(html).to.contain("format_decimal(1.01)                                                            # 1.01")

    expect(html).to.contain("translate('Title')                                                              # Title")

    expect(html).to.contain("pluralize('{0} article', '{0} articles', 0).format(0)                           # 0 articles")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 1).format(1)                           # 1 article")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 2).format(2)                           # 2 articles")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 3).format(3)                           # 3 articles")
    yield

    # fr-FR
    response = app.get(url, status=200, headers={'Accept-Language': 'fr-FR, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5'})

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain("date_utc = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)          # 2021-01-01 00:00:00+00:00")

    expect(html).to.contain("format_date(date_utc)                                                           # 1 janv. 2021")
    expect(html).to.contain("format_datetime(date_utc)                                                       # 1 janv. 2021 00:00:00")
    expect(html).to.contain("format_date(date_utc, format='long')                                            # 1 janvier 2021")
    expect(html).to.contain("format_datetime(date_utc, date_format='long')                                   # 1 janvier 2021 00:00:00")
    expect(html).to.contain("format_datetime(date_utc, date_format='short')                                  # 01/01/2021 00:00:00")
    expect(html).to.contain("format_datetime(date_utc, date_format='short', time_format='short')             # 01/01/2021 00:00")
    expect(html).to.contain("format_datetime(localtoutc(date_utc), date_format='short', time_format='short') # 01/01/2021 00:00")
    expect(html).to.contain("format_datetime(utctolocal(date_utc), date_format='short', time_format='short') # 01/01/2021 01:00")
    expect(html).to.contain("format_time(date_utc)                                                           # 00:00:00")
    expect(html).to.contain("format_time(date_utc, format='short')                                           # 00:00")

    expect(html).to.contain("format_decimal(1.01)                                                            # 1,01")

    expect(html).to.contain("translate('Title')                                                              # Titre")

    expect(html).to.contain("pluralize('{0} article', '{0} articles', 0).format(0)                           # 0 article")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 1).format(1)                           # 1 article")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 2).format(2)                           # 2 articles")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 3).format(3)                           # 3 articles")
    yield

    # invalid -> en
    response = app.get(url, status=200, headers={'Accept-Language': 'iv-IV, iv;q=0.9, iv;q=0.8, iv;q=0.7, *;q=0.5'})

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain("date_utc = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)          # 2021-01-01 00:00:00+00:00")

    expect(html).to.contain("format_date(date_utc)                                                           # Jan 1, 2021")
    expect(html).to.contain("format_datetime(date_utc)                                                       # Jan 1, 2021 12:00:00 AM")
    expect(html).to.contain("format_date(date_utc, format='long')                                            # January 1, 2021")
    expect(html).to.contain("format_datetime(date_utc, date_format='long')                                   # January 1, 2021 12:00:00 AM")
    expect(html).to.contain("format_datetime(date_utc, date_format='short')                                  # 1/1/21 12:00:00 AM")
    expect(html).to.contain("format_datetime(date_utc, date_format='short', time_format='short')             # 1/1/21 12:00 AM")
    expect(html).to.contain("format_datetime(localtoutc(date_utc), date_format='short', time_format='short') # 1/1/21 12:00 AM")
    expect(html).to.contain("format_datetime(utctolocal(date_utc), date_format='short', time_format='short') # 1/1/21 1:00 AM")
    expect(html).to.contain("format_time(date_utc)                                                           # 12:00:00 AM")
    expect(html).to.contain("format_time(date_utc, format='short')                                           # 12:00 AM")

    expect(html).to.contain("format_decimal(1.01)                                                            # 1.01")

    expect(html).to.contain("translate('Title')                                                              # Title")

    expect(html).to.contain("pluralize('{0} article', '{0} articles', 0).format(0)                           # 0 articles")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 1).format(1)                           # 1 article")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 2).format(2)                           # 2 articles")
    expect(html).to.contain("pluralize('{0} article', '{0} articles', 3).format(3)                           # 3 articles")
    yield


@test_steps('anonymous', 'None', 'UTC', 'Indian/Reunion', 'Invalid')
def test_timezone(app, app_a, app_1, app_2, app_3):
    """ Test function for timezone """

    url = app.route_path('i18n')

    # Anonymous -> Europe/Paris
    response = app.get(url, status=200)

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain('<a class="btn btn-primary me-3" href="/auth/sign-in" title="Sign in">Sign in</a>')
    expect(html).to.contain('get_tzinfo(request)                                                             # Europe/Paris')

    expect(html).to.contain('format_datetime(localize(date_utc), date_format=\'short\', time_format=\'short\')   # 1/1/21 1:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 1:00 AM')

    expect(html).to.contain('format_datetime(localize(date_loc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_loc), date_format=\'short\', time_format=\'short\') # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date), date_format=\'short\', time_format=\'short\')       # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date), date_format=\'short\', time_format=\'short\')     # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 1:00 AM')
    yield

    # Timezone None -> Europe/Paris
    response = app_a.get(url, status=200)

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain('<span class="navbar-text me-3">admin</span>')
    expect(html).to.contain('get_tzinfo(request)                                                             # Europe/Paris')

    expect(html).to.contain('format_datetime(localize(date_utc), date_format=\'short\', time_format=\'short\')   # 1/1/21 1:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 1:00 AM')

    expect(html).to.contain('format_datetime(localize(date_loc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_loc), date_format=\'short\', time_format=\'short\') # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date), date_format=\'short\', time_format=\'short\')       # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date), date_format=\'short\', time_format=\'short\')     # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 1:00 AM')
    yield

    # Timezone UTC
    response = app_1.get(url, status=200)

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain('<span class="navbar-text me-3">John Doe</span>')
    expect(html).to.contain('get_tzinfo(request)                                                             # UTC')

    expect(html).to.contain('format_datetime(localize(date_utc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date_loc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date), date_format=\'short\', time_format=\'short\')       # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 12:00 AM')
    yield

    # Timezone Indian/Reunion (UTC+4)
    response = app_2.get(url, status=200)

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain('<span class="navbar-text me-3">Jane Doe</span>')
    expect(html).to.contain('get_tzinfo(request)                                                             # Indian/Reunion')

    expect(html).to.contain('format_datetime(localize(date_utc), date_format=\'short\', time_format=\'short\')   # 1/1/21 4:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 4:00 AM')

    expect(html).to.contain('format_datetime(localize(date_loc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_loc), date_format=\'short\', time_format=\'short\') # 12/31/20 8:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date), date_format=\'short\', time_format=\'short\')       # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date), date_format=\'short\', time_format=\'short\')     # 12/31/20 8:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 4:00 AM')
    yield

    # Timezone Invalid -> Europe/Paris
    response = app_3.get(url, status=200)

    # Replace non-breaking spaces with regular spaces
    html = str(response.html).replace(' ', ' ')

    expect(html).to.contain('<span class="navbar-text me-3">user3</span>')
    expect(html).to.contain('get_tzinfo(request)                                                             # Europe/Paris')

    expect(html).to.contain('format_datetime(localize(date_utc), date_format=\'short\', time_format=\'short\')   # 1/1/21 1:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(utctolocal(date_utc), date_format=\'short\', time_format=\'short\') # 1/1/21 1:00 AM')

    expect(html).to.contain('format_datetime(localize(date_loc), date_format=\'short\', time_format=\'short\')   # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date_loc), date_format=\'short\', time_format=\'short\') # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date_loc), date_format=\'short\', time_format=\'short\') # 1/1/21 12:00 AM')

    expect(html).to.contain('format_datetime(localize(date), date_format=\'short\', time_format=\'short\')       # 1/1/21 12:00 AM')
    expect(html).to.contain('format_datetime(localtoutc(date), date_format=\'short\', time_format=\'short\')     # 12/31/20 11:00 PM')
    expect(html).to.contain('format_datetime(utctolocal(date), date_format=\'short\', time_format=\'short\')     # 1/1/21 1:00 AM')
    yield


@test_steps('get', 'post')
def test_validators(app):
    """ Test function for validators view """

    url = app.route_path('validators')

    # GET
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<a class="btn btn-primary me-3" href="/auth/sign-in" title="Sign in">Sign in</a>')
    expect(str(response.html)).to.contain('<h2>Validators</h2>')
    yield

    # POST
    params = {
        'checkbox_input': 'true',
        'upload_input': Upload('filename.txt', b'File content', 'application/octet-stream'),
    }

    response = app.post(url, params=params, status=200)

    expect(str(response.html)).to.contain('<a class="btn btn-primary me-3" href="/auth/sign-in" title="Sign in">Sign in</a>')
    expect(str(response.html)).to.contain('<h2>Validators</h2>')
    expect(str(response.html)).to.contain('\'checkbox_input\': True')
    expect(str(response.html)).to.contain('\'upload_input.name\': \'filename.txt\'')
    yield


@test_steps('anonymous', 'guest', 'admin_ok')
def test_apidoc(app, app_a, app_1):
    """ Test function for api-doc view """

    url = app.route_path('api-doc')

    # Anonymous
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('API documentation')
    expect(str(response.html)).to.contain('Articles API')
    expect(str(response.html)).to.contain('heading-api-articles-search')
    expect(str(response.html)).to.exclude('heading-api-articles-create')
    expect(str(response.html)).to.exclude('heading-api-articles-modify')
    expect(str(response.html)).to.exclude('heading-api-articles-delete')
    yield

    # Guest
    response = app_1.get(url, status=200)

    expect(str(response.html)).to.contain('API documentation')
    expect(str(response.html)).to.contain('Articles API')
    expect(str(response.html)).to.contain('heading-api-articles-search')
    expect(str(response.html)).to.contain('heading-api-articles-create')
    expect(str(response.html)).to.contain('heading-api-articles-modify')
    expect(str(response.html)).to.exclude('heading-api-articles-delete')
    yield

    # Admin
    response = app_a.get(url, status=200)

    expect(str(response.html)).to.contain('API documentation')
    expect(str(response.html)).to.contain('Articles API')
    expect(str(response.html)).to.contain('heading-api-articles-search')
    expect(str(response.html)).to.contain('heading-api-articles-create')
    expect(str(response.html)).to.contain('heading-api-articles-modify')
    expect(str(response.html)).to.contain('heading-api-articles-delete')
    yield
