""" Test functions for api.articles module """

from pytest_steps import test_steps

from robber import expect

# pylint: disable=no-member


def extract_result(response, status_code, status):
    """  Extract result from response """

    expect(response.status_code).to.eq(status_code)

    try:
        result = response.json
    except ValueError:
        result = None

    expect(result).to.be.a.dict()
    expect(result).to.contain('apiVersion', 'method', 'result')
    expect(result['apiVersion']).to.eq('1.0')

    if status:
        expect(result['result']).to.be.true()
    else:
        expect(result['result']).to.be.false()

    return result


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'duplicate', 'invalid')
def test_create(app, app_1, app_a):
    """ Test function for api.articles.create view """

    url = app.route_path('api.articles.create')

    # Anonymous
    response = app.post(url)

    expect(response.status_code).to.eq(302)
    expect(response.headers.get('location')).to.contain('http://localhost/auth/sign-in')
    yield

    # Guest OK
    data = {
        'title': 'Test article #1',
        'status': 'draft',
        'text': 'text',
    }

    response = app_1.post(url, params=data)

    extract_result(response, 201, True)
    yield

    # Admin OK
    data = {
        'title': 'Test article #2',
        'status': 'draft',
        'text': 'text',
    }

    response = app_a.post(url, params=data)

    extract_result(response, 201, True)
    yield

    # Duplicate
    response = app_a.post(url, params=data, expect_errors=True)
    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.match('Title already used in another article')
    yield

    # Invalid
    response = app_1.post(url, params={'text': None}, expect_errors=True)
    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.contain('Missing value')
    yield


@test_steps('asc', 'desc', 'page-1', 'page-2', 'limit-50', 'limit-100')
def test_pager(app):
    """ Test function for pager """

    params = {
        'status': 'published',
        'articles.limit': 10,
        'articles.page': 1,
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    # ASC
    params_ = params.copy()
    params_['articles.page'] = '1'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(len(result['articles']['items'])).to.eq(10)
    expect(result['articles']['items'][0]['id']).to.eq(1)
    yield

    # DESC
    params_ = params.copy()
    params_['articles.order'] = 'desc'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(len(result['articles']['items'])).to.eq(10)
    expect(result['articles']['items'][0]['id']).to.eq(99)
    yield

    # page-1
    params_ = params.copy()
    params_['articles.page'] = '1'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(len(result['articles']['items'])).to.eq(10)
    expect(result['articles']['items'][0]['id']).to.eq(1)
    yield

    # page-2
    params_ = params.copy()
    params_['articles.page'] = '2'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(2)
    expect(len(result['articles']['items'])).to.eq(10)
    expect(result['articles']['items'][0]['id']).to.eq(11)
    yield

    # limit-50
    params_ = params.copy()
    params_['articles.limit'] = '50'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(len(result['articles']['items'])).to.eq(50)
    yield

    # limit-100
    params_ = params.copy()
    params_['articles.limit'] = '100'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(len(result['articles']['items'])).to.eq(99)
    yield


@test_steps('id', 'creation_date', 'modification_date', 'title', 'status')
def test_order(app):
    """ Test function for order """

    params = {
        'status': 'published',
        'articles.limit': 10,
        'articles.page': 1,
        'articles.order': 'asc',
    }

    for sort_ in ('id', 'creation_date', 'modification_date', 'title', 'status'):
        params['articles.sort'] = sort_

        url = app.route_path('api.articles.search', _query=params)
        response = app.get(url)
        result = extract_result(response, 200, True)

        expect(result['articles']['pager']['sort']).to.eq(sort_)
        yield


@test_steps('normal', 'excluded_ids', 'selected_ids', 'title', 'text', 'term', 'exact', 'invalid')
def test_search(app):
    """ Test function for api.articles.search view """

    # Normal
    params = {
        'status': 'published',
        'articles.limit': 10,
        'articles.page': 1,
        'articles.sort': 'id',
        'articles.order': 'asc',
    }

    url = app.route_path('api.articles.search', _query=params)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['page']).to.eq(1)
    expect(result['articles']['pager']['count']).to.eq(10)
    expect(result['articles']['pager']['pages']).to.eq(10)
    expect(result['articles']['pager']['total']).to.eq(99)
    yield

    # Excluded ids
    params_ = params.copy()
    params_['excluded_ids'] = '1, 2'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(97)
    yield

    # Selected ids
    params_ = params.copy()
    params_['selected_ids'] = '1, 2'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(2)
    yield

    # Title
    params_ = params.copy()
    params_['title'] = 'Article #1'  # 1, 10-19

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(11)
    yield

    # Text
    params_ = params.copy()
    params_['text'] = 'Text of article #2'  # 2, 20-29

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(11)
    yield

    # Term
    params_ = params.copy()
    params_['term'] = 'Article #3'  # 3, 30-39

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(11)
    yield

    # Exact
    params_ = params.copy()
    params_['term'] = 'Article #4'  # 4
    params_['exact'] = '1'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['articles']['pager']['total']).to.eq(1)
    yield

    # Invalid
    params_ = params.copy()
    params_['status'] = 'invalid'

    url = app.route_path('api.articles.search', _query=params_)
    response = app.get(url, expect_errors=True)
    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('status')
    expect(result['errors']['status']).to.contain('Value must be one of')
    yield


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'invalid', 'not_found')
def test_modify(app, app_1, app_a):
    """ Test function for api.articles.modify view """

    url = app.route_path('api.articles.modify', article=101)

    # Anonymous
    data = {
        'title': 'Article modified #2',
        'status': 'draft',
        'text': 'modified !',
    }
    response = app.put(url, params=data)

    expect(response.status_code).to.eq(302)
    expect(response.headers.get('location')).to.contain('http://localhost/auth/sign-in')
    yield

    # Guest OK
    response = app_1.put(url, params=data)
    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('modified !')
    yield

    # Admin OK
    data['text'] = 'modified ! again !'
    response = app_a.put(url, params=data)
    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('modified ! again !')
    yield

    # Invalid
    response = app_a.put(url, expect_errors=True)
    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('title')
    expect(result['errors']['title']).to.contain('Missing value')
    yield

    # Not Found
    url = app.route_path('api.articles.modify', article=0)
    response = app_a.put(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'invalid', 'not_found')
def test_status(app, app_1, app_a):
    """ Test function for api.articles.status view """

    url = app.route_path('api.articles.status', article=101)

    # Anonymous
    response = app.put(url, params={'status': 'refused'})

    expect(response.status_code).to.eq(302)
    expect(response.headers.get('location')).to.contain('http://localhost/auth/sign-in')
    yield

    # Guest OK
    response = app_1.put(url, params={'status': 'refused'})
    result = extract_result(response, 200, True)

    expect(result['article']['status']).to.eq('refused')
    yield

    # Admin OK
    response = app_a.put(url, params={'status': 'draft'})
    result = extract_result(response, 200, True)

    expect(result['article']['status']).to.eq('draft')
    yield

    # Invalid
    response = app_a.put(url, params={'status': 'invalid'}, expect_errors=True)
    result = extract_result(response, 400, False)

    expect(result['errors']).to.contain('status')
    expect(result['errors']['status']).to.contain('Value must be one of: draft; published; refused')
    yield

    # Not Found
    url = app.route_path('api.articles.status', article=0)
    response = app_a.put(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield


@test_steps('article1', 'article2', 'not_found')
def test_visual(app):
    """ Test function for api.articles.visual view """

    # Article #1
    url = app.route_path('api.articles.visual', article=1)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('Text of article #1')
    expect(result['article']['status']).to.eq('published')
    yield

    # Article #2
    url = app.route_path('api.articles.visual', article=2)
    response = app.get(url)
    result = extract_result(response, 200, True)

    expect(result['article']['text']).to.eq('Text of article #2')
    expect(result['article']['status']).to.eq('published')
    yield

    # Not Found
    url = app.route_path('api.articles.visual', article=0)
    response = app.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield


@test_steps('anonymous', 'guest', 'admin_ok', 'not_found')
def test_delete(app, app_1, app_a):
    """ Test function for api.articles.delete view """

    url = app.route_path('api.articles.delete', article=103)

    # Anonymous
    response = app.delete(url)

    expect(response.status_code).to.eq(302)
    expect(response.headers.get('location')).to.contain('http://localhost/auth/sign-in')
    yield

    # Guest
    response = app_1.delete(url, expect_errors=True)

    expect(response.status_code).to.eq(403)
    yield

    # Admin OK
    response = app_a.delete(url)

    extract_result(response, 200, True)
    yield

    # Not Found
    url = app.route_path('api.articles.delete', article=0)
    response = app_a.delete(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    yield
