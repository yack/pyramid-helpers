""" Test functions for articles views """

from pytest_steps import test_steps

from robber import expect


# pylint: disable=no-member


@test_steps('anonymous', 'guest', 'admin', 'partial', 'csv')
def test_search(app, app_1, app_a):
    """ Test function for search view """

    url = app.route_path('articles.search')

    # Anonymous
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<a class="btn btn-primary me-3" href="/auth/sign-in" title="Sign in">Sign in</a>')
    expect(str(response.html)).to.contain('<h2>Articles</h2>')
    expect(str(response.html)).to.contain('article from 1 to 10 on 99')
    yield

    # Guest
    response = app_1.get(url, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">John Doe</span>')
    yield

    # Admin
    response = app_a.get(url, status=200)

    expect(str(response.html)).to.contain('<span class="navbar-text me-3">admin</span>')
    yield

    # Partial
    url = app.route_path('articles.search', _query={'articles.partial': 1})
    response = app.get(url, status=200)

    expect(str(response.html)).to.exclude('<h2>Articles</h2>')
    expect(str(response.html)).to.contain('article from 1 to 10 on 99')
    yield

    # CSV
    url = app.route_path('articles.search', _query={'csv': 1})
    response = app.get(url, status=200)

    expect(str(response.text)).to.contain('id;title;creation date;status;text')
    yield


@test_steps('visual_ok', 'not_found')
def test_visual(app):
    """ Test function for visual view """

    # Visual OK
    url = app.route_path('articles.visual', article=1)
    response = app.get(url, status=200)

    expect(str(response.html)).to.contain('<p>Text of article #1</p>')
    yield

    # Not found
    url = app.route_path('articles.visual', article=0)
    response = app.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    expect(str(response.html)).to.contain('Invalid article')
    yield


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'errors')
def test_create(app, app_1, app_a):
    """ Test function for create view """

    url = app.route_path('articles.create')

    # Anonymous
    response = app.get(url, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/auth/sign-in?redirect=%2Farticles%2Fcreate')
    yield

    # Guest OK
    response = app_1.get(url, status=200)

    expect(str(response.html)).to.contain('<h2 class="card-title h5 pt-2">New article</h2>')
    yield

    # Admin OK
    data = {
        'title': 'Article #100',
        'status': 'draft',
        'text': 'text',
    }

    response = app_a.post(url, params=data, status=302)

    expect(response.headers.get('location')).to.match(r'http://localhost/articles/\d+')
    yield

    # Errors
    response = app_1.post(url, status=200)

    expect(str(response.html)).to.contain('<h2 class="card-title h5 pt-2">New article</h2>')
    expect(str(response.html)).to.contain('Missing value')
    yield


@test_steps('anonymous', 'guest_ok', 'admin_ok', 'errors', 'not_found')
def test_modify(app, app_1, app_a):
    """ Test function for modify view """

    url = app.route_path('articles.modify', article=100)

    # Anonymous
    response = app.get(url, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/auth/sign-in?redirect=%2Farticles%2F100%2Fmodify')
    yield

    # Guest OK
    response = app_1.get(url, status=200)

    expect(str(response.html)).to.contain('<h2 class="card-title h5 pt-2">Article "Article to modify #1" edition</h2>')
    yield

    # Admin OK
    data = {
        'title': 'Article modified #1',
        'status': 'draft',
        'text': 'modified !',
    }

    response = app_a.post(url, params=data, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/articles/100')
    yield

    # Errors
    response = app_1.post(url, status=200)

    expect(str(response.html)).to.contain('<h2 class="card-title h5 pt-2">Article "Article modified #1" edition</h2>')
    expect(str(response.html)).to.contain('Missing value')
    yield

    # Not found
    url = app.route_path('articles.modify', article=0)
    response = app_1.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    expect(str(response.html)).to.contain('Invalid article')
    yield


@test_steps('anonymous', 'guest', 'admin_ok', 'cancel', 'delete', 'not_found')
def test_delete(app, app_1, app_a):
    """ Test function for create view """

    url = app.route_path('articles.delete', article=102)

    # Anonymous
    response = app.get(url, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/auth/sign-in?redirect=%2Farticles%2F102%2Fdelete')
    yield

    # Guest
    response = app_1.get(url, expect_errors=True)

    expect(response.status_code).to.eq(403)
    expect(str(response.html)).to.contain('<strong>Error:</strong> Unauthorized: delete failed permission check')
    yield

    # Admin OK
    response = app_a.get(url, status=200)

    expect(str(response.html)).to.contain('Do you really want to delete article "Article to remove #1" ?')
    yield

    # Cancel
    response = app_a.post(url, params={'cancel': 1}, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/articles/102')
    yield

    # Delete
    response = app_a.post(url, status=302)

    expect(response.headers.get('location')).to.eq('http://localhost/articles')
    yield

    # Not found
    url = app.route_path('articles.delete', article=0)
    response = app_a.get(url, expect_errors=True)

    expect(response.status_code).to.eq(404)
    expect(str(response.html)).to.contain('Invalid article')
    yield
